from pymongo import MongoClient
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import json

'''
  A simple script to capture tweets from the Twitter Streaming API and store
  them in a MongoDB database.

  Makes use of the tweepy library (https://github.com/tweepy/tweepy)
'''

# Twitter Authentication - OAuth (get these details from dev.twitter.com)
consumer_key = ''
consumer_secret = ''
access_token_key = ''
access_token_secret = ''

client = MongoClient()
db = client.mongo_database_name  # Set database name here.
tweets = db.tweets               # Assuming a collection named tweets

counter = 0

# Tweepy StreamListener class
class StdOutListener(StreamListener):

  def on_data(self, data):
    global counter
    counter += 1

    jdata = json.loads(data)

    print str(counter)
    tweet_id = tweets.insert(jdata)
    return True

  def on_error(self, status):
    print status


if __name__ == '__main__':
  l = StdOutListener()
  auth = OAuthHandler(consumer_key, consumer_secret)
  auth.set_access_token(access_token_key, access_token_secret)
  stream = Stream(auth, l)
  # Enter your search strings and hashtags here
  stream.filter(track=['search', '#hashtag'])
